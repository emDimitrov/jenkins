package jenkins;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.fmi.uni.config.DataPopulator;

public class TestDataPopulator {
	@Test
	public void testPopulateUsers() {
		DataPopulator dataPopulator = new DataPopulator();
		
		assertEquals(3, dataPopulator.populateUsers().size());
	}

}
