package jenkins;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.fmi.uni.service.AuthenticationService;

public class TestAuthenticationService {
	
	@Test
	public void testAuthenticate() {
		AuthenticationService service = new AuthenticationService();
		
		assertEquals("Hello emil!", service.login("emil","123456"));
		assertEquals("Hello ivan!", service.login("ivan","456"));
		assertEquals("Hello petkan!", service.login("petkan","324"));
	}
}
