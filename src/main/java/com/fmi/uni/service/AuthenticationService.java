package com.fmi.uni.service;

import java.util.List;
import java.util.Optional;

import com.fmi.uni.config.DataPopulator;
import com.fmi.uni.entity.User;

public class AuthenticationService {
	private List<User> users;
		
	public AuthenticationService() {
		this.users = DataPopulator.populateUsers();
	}
	
	public String login(String username, String password) {
		String message = "Wrong username or password";
		boolean isAuthenticated = authenticate(username, password);
		if(isAuthenticated) {
			message = "Hello " + username + "!";
		}
		
		return message;
	}
	
	private boolean authenticate(String username, String password) {
		Optional<User> user = users.stream().filter(u -> u.getUsername().equals(username)).findFirst();
		if(user.isPresent() && user.get().getPassword().equals(password)) {
			return true;
		}
		
		return false;
	}
}
