package com.fmi.uni;

import java.util.Scanner;

import com.fmi.uni.service.AuthenticationService;

public class Program {

	public static void main(String[] args) {
		
		System.out.println("------------LOGIN------------");
		try(Scanner scanner = new Scanner(System.in)){
			System.out.print("Username: ");
			String username = scanner.nextLine();
			
			System.out.print("Password: ");
			String password = scanner.nextLine();
			
			AuthenticationService auth = new AuthenticationService();
			String userMessage = auth.login(username, password);
			
			System.out.println(userMessage);
		}
		
	}
	
}
