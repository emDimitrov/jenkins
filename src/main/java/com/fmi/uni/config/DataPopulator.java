package com.fmi.uni.config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fmi.uni.entity.User;

public class DataPopulator {
	
	public static List<User> populateUsers() {
		List<User> users = new ArrayList<User>();
		String file = "src/main/resources/users.txt";
		try(BufferedReader reader = new BufferedReader(new FileReader(file))){
			
			String line;
			while((line = reader.readLine()) != null) {
				String[] userInfo = line.split(",");
				User user = new User(userInfo[0], userInfo[1], userInfo[2]);
				users.add(user);
			}
			
			return users;
			
		} catch (FileNotFoundException e) {
			System.out.println("File for populating user data is not found");
			return new ArrayList<>();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ArrayList<>();
		}
	}
}
